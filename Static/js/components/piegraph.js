export default {
  extends: VueChartJs.Pie,
  props: ['Rsize', "Gsize", "Bsize"],
  mounted() {
    const chartdata = {
      labels: ['Traductions', 'Synonymes', 'Variations'],
      datasets: [{
        label: 'Dataset 1',
        data: [this.Rsize, this.Gsize, this.Bsize],
        backgroundColor: ['rgba(255, 0, 0, 0.4)',
          'rgba(0, 255, 0, 0.4)',
          'rgba(0, 0, 255, 0.4)',
        ],
      }]
    }
    const options = {
      responsive: true,
      maintainAspectRatio: false
    }
    this.renderChart(chartdata, options)
  }
}