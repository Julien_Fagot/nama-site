import Piegraph from "./piegraph.js"
var piegraph = Vue.component = ("piegraph", Piegraph)
export default {
  props: ['item'],
  data: function() {
    return {
      reflanguage: "b",
      reference: [],
      variations: [],
      translations: [],
      synonyms: [],
      variationsnum: 0,
      traductionsnum: 0,
      synonymsnum: 0,
      totalvariantes: 0
    }
  },
  components: {
    piegraph: piegraph
  },
  methods: {
    getdata() {
      this.item.form.forEach((forme) => {
        if (forme['_type'] == 'reference') {
          this.reflanguage = forme["_xml:lang"]
          this.reference = forme
        } else if (forme['_xml:lang'] == this.reflanguage) {
          this.variations.push(forme.orth)
        } else if (forme['_xml:lang'] != this.reflanguage) {
          this.translations.push(forme.orth)
        }
        if (typeof forme.lbl != 'undefined') {
          this.synonyms.push(forme.lbl["__text"])
        }
      });
      this.variationsnum = this.variations.length
      this.traductionsnum = this.translations.length
      this.synonymsnum = this.synonyms.length
      this.totalvariantes = this.variationsnum + this.traductionsnum + this.synonymsnum
    }
  },
  beforeMount() {
    this.getdata()
  },
  template: `
  <tr>
  <th scope="row">{{item["_xml:id"]}}</th>
  <template v-for="form in item.form">
    <td v-if="form['_type']=='reference'"> {{form.orth}},{{form["_xml:lang"]}}</td>
  </template>
  <td>
    <div v-for="form in item.form">
      <div v-if="form['_type']!='reference'&&form['_xml:lang']==reflanguage" id="variation"> {{form.orth}},{{form["_xml:lang"]}}</div>
      <div v-if="form['_type']!='reference'&&form['_xml:lang']!=reflanguage" id="translation"> {{form.orth}},{{form["_xml:lang"]}}</div>
      <div v-if="typeof form.lbl!='undefined'" id="synonym">{{form.lbl["__text"]}},{{form["_xml:lang"]}}</div>
    </div>
    <div>{{totalvariantes}}</div>
    <div v-for="form in this.reference"></div>
  </td>
  <td>
  <!--
  Testing dump
  TODO
  Reference: {{this.reference}}
  <br>
  Variations: {{this.variations}}
  <br>
  Traductions: {{this.translations}}
  <br>
  Synonymes: {{this.synonyms}}
  <br>
  -->
    Graphe:
    <div>
    <piegraph :styles="{height:'145px'}" :Rsize=traductionsnum :Gsize=synonymsnum :Bsize=variationsnum></piegraph>
    </div>

  </td>
  </tr>
  `
}