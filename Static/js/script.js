import Tableline from "./components/tableline.js"
import Piegraph from "./components/piegraph.js"

var tableline = Vue.component = ("tableline", Tableline)
var piegraph = Vue.component = ("piegraph", Piegraph)
var appli = new Vue({

  el: "#vue",
  data: {
    datalist: [],
    texttest: "Ceci est un test",
    language: "",
  },
  components: {
    tableline: tableline,
    piegraph: piegraph
  },
  methods: {
    loadjson() {
      var that = this
      const xhttp = new XMLHttpRequest();
      xhttp.onload = function() {
        var parsedjson = JSON.parse(this.responseText).tei_part.entry
        that.datalist = parsedjson
        /*parsedjson.forEach((item) => {
          var reference= ""
          item.form.forEach((form) => {
            if form["_type"]=="reference"
          })
          that.datalist.push({
            "ref":
            "form": item.form,
            "def": item.def,
            "id": item["_xml:id"].split('').slice(-1)[0]
          })
        });*/
        /*if needed, can modify datalist format there*/
      }
      xhttp.open("GET", "NAMA_tei.json", true)
      xhttp.send()
    }
  },
  beforeMount() {
    this.loadjson()
  },
})
/*    appli.component("tableline", Tableline) appli.component("piegraph", Piegraph)



    appli.mount('#vue')*/